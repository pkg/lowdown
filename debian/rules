#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk

# output every command that modifies files on the build system.
#export DH_VERBOSE = 1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# Use libbsd for a system copy of the BSD functions, instead of upstream's
# embedded compats.c coming from the oconfigure project.
# Also see https://github.com/kristapsdz/oconfigure/issues/19
CFLAGS += $(shell pkg-config --cflags libmd libbsd-overlay)
LDFLAGS += $(shell pkg-config --libs libmd libbsd-overlay)

# avoid symbol leakage; keep this in the package until merged upstream:
# see https://github.com/kristapsdz/lowdown/issues/109
LDFLAGS += -Wl,--version-script=debian/liblowdown.map

%:
	dh $@

override_dh_auto_configure:
	CFLAGS="${CFLAGS}" \
	./configure \
		PREFIX=/usr \
		MANDIR="/usr/share/man" \
		LIBDIR=/usr/lib/${DEB_HOST_MULTIARCH} \
		CPPFLAGS="${CPPFLAGS}" \
		LDFLAGS="${LDFLAGS}"

override_dh_auto_install:
	# make install only installs the main binary, not the library
	make install DESTDIR=debian/tmp
	make install_static DESTDIR=debian/tmp
	make install_shared DESTDIR=debian/tmp

override_dh_auto_test:
	make regress

override_dh_auto_clean:
	echo > Makefile.configure
	dh_auto_clean
