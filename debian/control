Source: lowdown
Priority: optional
Maintainer: Faidon Liambotis <paravoid@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libbsd-dev,
 libmd-dev,
 pkgconf | pkg-config,
Standards-Version: 4.6.2
Section: text
Homepage: https://kristaps.bsd.lv/lowdown/
Vcs-Browser: https://salsa.debian.org/debian/lowdown
Vcs-Git: https://salsa.debian.org/debian/lowdown.git
Rules-Requires-Root: no

Package: lowdown
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Simple markdown translator
 Lowdown is a Markdown translator producing HTML5, roff documents in the ms and
 man formats, LaTeX, gemini, and terminal output.
 .
 Beyond traditional Markdown syntax support, lowdown supports the following
 Markdown features and extensions:
  * autolinking
  * fenced code
  * tables
  * superscripts
  * footnotes
  * disabled inline HTML
  * “smart typography”
  * metadata
  * commonmark (in progress)
  * definition lists
  * extended attributes
  * task lists

Package: liblowdown-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liblowdown1 (= ${binary:Version}), ${misc:Depends}
Description: Simple markdown translator (development files)
 Lowdown is a Markdown translator producing HTML5, roff documents in the ms and
 man formats, LaTeX, gemini, and terminal output.
 .
 Beyond traditional Markdown syntax support, lowdown supports the following
 Markdown features and extensions:
  * autolinking
  * fenced code
  * tables
  * superscripts
  * footnotes
  * disabled inline HTML
  * “smart typography”
  * metadata
  * commonmark (in progress)
  * definition lists
  * extended attributes
  * task lists
 .
 This package contains development libraries, header files, and manpages.

Package: liblowdown1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Simple markdown translator (shared library)
 Lowdown is a Markdown translator producing HTML5, roff documents in the ms and
 man formats, LaTeX, gemini, and terminal output.
 .
 Beyond traditional Markdown syntax support, lowdown supports the following
 Markdown features and extensions:
  * autolinking
  * fenced code
  * tables
  * superscripts
  * footnotes
  * disabled inline HTML
  * “smart typography”
  * metadata
  * commonmark (in progress)
  * definition lists
  * extended attributes
  * task lists
 .
 This package contains the shared runtime library.
